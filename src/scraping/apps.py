from django.apps import AppConfig


class ScrapingConfig(AppConfig):
    name = 'scraping'
    verbose_name = 'Застосунок зі збирання вакансій'
